var http = require('http');
var fs = require('fs');
var url = require('url');

var availableViews = [];
var availableScripts = [];
var availableStyleSheets = [];
var availableFiles = [];
var requests = 0;
var messages = [];

function log (data) {
	if (data) {
		console.log(data);
	}
}

fs.readdir(__dirname + '/views/', function collectAvailableFiles (err, files) {
	log(err);
	
	availableViews = files;
	availableFiles = availableFiles.concat(availableViews);
});

fs.readdir(__dirname + '/scripts/', function collectAvailableFiles (err, files) {
	log(err);
	
	availableScripts = files;
	availableFiles = availableFiles.concat(availableScripts);
});

fs.readdir(__dirname + '/css/', function collectAvailableFiles (err, files) {
	log(err);
	
	availableStyleSheets = files;
	availableFiles = availableFiles.concat(availableStyleSheets);
});

function fileNotFound (res) {
	var notFound = 'Not Found.';
	res.writeHead(404, { 'Content-Type': 'text/html', 'Content-Length': notFound.length });
	res.write(notFound + '\n');
	res.end();
}

function readFile (file, dir, type, res) {
	fs.readFile(__dirname + dir + file, function (err, data) {
		log(err);
		res.writeHead(200, { 'Content-Type': type, 'Content-Length': data.length });
		res.write(data);
		res.end();
	});
}

function logRequest (req) {
	requests++;
	console.log('Request#: ' + requests + '. Resource requested: ' + url.parse(req.url).pathname);
}

function dynamicResponse (req, res) {
	req.on('data', function handleMessage (chunk) { 
		var parsedChunk = JSON.parse(chunk);
		if (parsedChunk.message.length > 0) {
			var message = { time: new Date(), name: parsedChunk.name, message: parsedChunk.message };
			messages.push(message);
			log(parsedChunk.name + ' said: ' + parsedChunk.message + ' @ ' + message.time);
		}
		var response = JSON.stringify( { messages: messages, messageCount: messages.length } );
		res.writeHead(200, { 'Content-Type': 'application/json', 'Content-Length': response.length });
		res.write(response);
		res.end();
	});
}

function updateClients (req, res) {
	var response = JSON.stringify({ messages: messages, messageCount: messages.length });
	res.writeHead(200, { 'Content-Type': 'application/json', 'Content-Length': response.length });
	res.write(response);
	res.end();
}

http.createServer(function handleRequests (req, res) {
	var fileName = url.parse(req.url).pathname.substring(1);
	
	if (fileName === 'q') {
		dynamicResponse(req, res);
		return;
	} else if (fileName === 'r') {
		updateClients(req, res);
		return;
	}

	var fileExt = fileName.substring(fileName.indexOf('.'));
	var fileFound = false;
	
	logRequest(req);
	
	for (var i=0; i < availableFiles.length; i++) {
		if (availableFiles[i] === fileName) {
			fileFound = true;
		}
	}

	if (fileFound) {
		switch (fileExt) {
			case '.html':
				readFile(fileName, '/views/', 'text/html', res);
				break;
			case '.js':
					readFile(fileName, '/scripts/', 'application/javascript', res);
				break;
			case '.css':
					readFile(fileName, '/css/', 'text/css', res);
				break;
			default:
				fileNotFound(res);
		}
	} else {
		fileNotFound(res);
	}
	

}).listen(process.env.C9_PORT, '0.0.0.0', function () { console.log('Server is listening on 127.0.0.1:4000');});