function Client(page) {
	var sentMessages = 0;
	var intervalId = 0;
	var self = this;
	
	this.setup = function () {
		$('#submit').click(this.sendMessage);
    $('#handle-submit').click(page.enterName);
    $('#stop').click(this.stopPolling);
	};
  
  this.stopPolling = function () {
    clearInterval(intervalId);
    intervalId = 0;
  };
	
	this.sendMessage = function () {
		$.post('/q', JSON.stringify( { name: $('#handle').val(), message: page.messageToSend() } ), page.renderUpdate);
		$('#sent-count').html(++sentMessages);
		self.update();
	};
	
	this.update = function () {
		if (intervalId === 0) {
			intervalId = setInterval(function getUpdates () { $.getJSON('/r', page.renderUpdate); }, 5000);
		}
	};
}

function Page () {
	this.messageToSend = function () { return $('#message').val(); };
  
  this.enterName = function () { 
    var handle = $('#handle').val();
    if (handle === '') {
      alert('Enter your handle...');
    } else {
      $('.handle').hide();
      $('.messages').show();
      $('#name').html(handle);
    }
  };
	
	this.renderUpdate = function (data) {
		$.each(data, function (key, value) {
			if (key === 'messages') {
				$('#messages').html('<p class="last-message"></p>');
				for (var i =0; i < value.length; i++) {
          var time = value[i].time;
          var dated = new Date(Date.parse(time));
          var date = dated.getHours() + ':' + dated.getMinutes();
					$('.last-message').before('<p>' + value[i].name + ' @ ' + date + ' said: ' + value[i].message + '</p>');
					$('.last-message').removeClass('last-message');
					$('#messages p').first().addClass('last-message');					
				}
			}	else if (key === 'messageCount') {
				$('#message-count').html(value);
			}
		});
		$('#message-count').html(data.messageCount);
	};
}

$(document).ready(function () {
	var page = new Page();
	var client = new Client(page);
	client.setup();
});